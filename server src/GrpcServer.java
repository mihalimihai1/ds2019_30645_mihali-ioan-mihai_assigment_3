package com.example.springdemo;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import com.example.springdemo.services.HelloServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


import java.io.IOException;

@Configuration
public class GrpcServer {

    @Autowired
    private HelloServiceImpl intakeService;

    @Bean
    public void start() throws IOException, InterruptedException {

        Server server = ServerBuilder
                .forPort(9090)
                .addService(intakeService).build();

        server.start();
        System.out.println("Server started at " + server.getPort());
        server.awaitTermination();
    }
}
