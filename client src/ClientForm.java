package org.baeldung.grpc;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.Timer;

public class ClientForm extends JFrame {
    public JButton button1;
    public JButton stop;
    public JPanel panel1;
    public JLabel label;
    private JTable table1 = new JTable();
    private DefaultTableModel dlm = (DefaultTableModel) table1.getModel();
    public Timer timer;
    public JTextArea textArea1;
    public int k = 0;
    public DateFormat dateformat;

    HelloServiceGrpc.HelloServiceBlockingStub stub;

    public ClientForm(final HelloServiceGrpc.HelloServiceBlockingStub stub) {
        this.stub = stub;
        this.createUIComponents();
        this.init();

        button1.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                timer = new Timer(500, new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        k++;
                        if (k < 100000) {
                            label.setText(Integer.toString(k));
                        } else {
                            ((Timer) (e.getSource())).stop();
                        }
                    }
                });
                timer.setInitialDelay(0);
                timer.start();

                    try {
                        int patientId = Integer.parseInt(textArea1.getText());

                        IntakeRequest displayRequest = IntakeRequest.newBuilder().setIdPatient(patientId).build();
                        IntakePlan response = stub.display(displayRequest);

                        IntakePlan intakePlan = stub.display(IntakeRequest.newBuilder()
                                .setIdPatient(patientId)
                                .build());

                        Object[] date = new Object[3];

                        dlm.setRowCount(0);
                        dlm.setColumnCount(0);

                        dlm.addColumn("Id");
                        dlm.addColumn("Start");
                        dlm.addColumn("End");

                        date[0] = response.getIntake(0).getIntakeId();
                        date[1] = response.getIntake(0).getStart();
                        date[2] = response.getIntake(0).getEnd();
                        dlm.addRow(date);


                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(null, "Errr", ex.getMessage(), JOptionPane.ERROR_MESSAGE);
                    }
                }
        });

    }

    private void init() {
        this.setContentPane(this.panel1);
        this.setVisible(true);
        this.setSize(new Dimension(400, 400));
    }

    private void createUIComponents() {
        this.panel1 = new JPanel();
        this.textArea1 = new JTextArea(10, 10);
        this.label = new JLabel();
        this.button1 = new JButton("Select patient id");
        this.stop = new JButton("Stop");

        this.panel1.setLayout(new GridLayout(2,2));
        this.panel1.add(this.button1);
        this.panel1.add(new JScrollPane(table1));
        this.panel1.add(this.textArea1);
        this.panel1.add(this.label);
    }
}
