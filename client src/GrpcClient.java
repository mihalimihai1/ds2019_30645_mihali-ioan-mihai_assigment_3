package org.baeldung.grpc;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import javax.swing.*;
import java.io.IOException;
import java.util.TimerTask;

public class GrpcClient {

    public static void main(String[] args) throws IOException,  InterruptedException{
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 9090)
                .usePlaintext()
                .build();

        HelloServiceGrpc.HelloServiceBlockingStub stub
                = HelloServiceGrpc.newBlockingStub(channel);

        System.out.println(response.getIntakeList());
        ClientForm frame = new ClientForm(stub);
    }
}
